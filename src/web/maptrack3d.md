# MapTrack3D

## Grafana
[Grafana](https://grafana.com/grafana/) is an analytics and visulisation platform

## MapTrack3D plugin

This is a plugin for Grafana that I created to visualize GPS points on a 3D globe.
The plugin was developed primarily to show the position and altitude of satellites in LEO.

![](https://raw.githubusercontent.com/flaminggoat/map-track-3-d/master/src/img/screenshot.png)

It has been developed using react and threeJS.
