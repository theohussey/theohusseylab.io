# Summary

- [Theo Hussey](./theo_hussey.md)

- [Projects]()
    - [Embedded]()
        - [Eachine E58 custom firware]()
        - [Embedded Graphics Menu](./embedded/embedded-graphics-menu.md)
        - [U-Boot on Zynq - Part 1](./embedded/uboot-on-zynq.md)
    - [Web]()
        - [MapTrack3D Grafana plugin](./web/maptrack3d.md)
