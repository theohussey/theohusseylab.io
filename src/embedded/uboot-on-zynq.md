# U-Boot on a Custom Zynq board - Part 1

30/10 2020

![](zynq.jpg)

## Intro

I wanted to set up my Zynq 7030 to boot linux using U-Boot and the U-Boot SPL (which is a first stage bootloader). The SPL is another option on the Zynq 7000 instead of using the Xilinx FSBL.


## Setting up U-Boot

U-Boot can be downloaded from the [Github repo](https://github.com/u-boot/u-boot). Once cloned you can load the defconfig for your board, if one exists. In my case I loaded the zynq_cse_qspi_defconfig as it should be similar to what I need.

You will need to set up the device tree for your board. For now I made a copy of the picozed device tree at `arch/arm/dts/zynq-picozed.dts` and changed the serial port to uart0 which is used on my board. Edit the Zynq section in the makefile at `arch/arm/dts/Makefile` and add a line for your new device tree.

Vivado is used to generate some board specific source files which are used in U-Boot. Open your project and go to the tcl console. Run the following to generate the files.
```
cd <project location>
write_hwdef <project_name>.hdf
hsi::open_hw_design <project_name>.hdf
hsi::get_hw_files
```
You should now have `ps7_init_gpl.c` and `ps7_init_gpl.h` in your project folder. Make a new folder in `board/xilinx/zynq` with the same name as your custom device tree. Copy the `ps7_init_gpl.c` and `ps7_init_gpl.h` files here.

Run `make menuconfig` to change the build configuration to use the files we have added. Set `CONFIG_DEFAULT_DEVICE_TREE` to use your custom device tree and initialization files. This is located at `Device Tree control -> Default Device Tree for DT Control`.

You can now compile U-Boot. You will find the SPL binary that can be placed on the SD card in `spl/boot.bin` I chose to put just the SPL on the card to check that that it was working in isolation.

## Debugging with GDB

When I first put the boot.bin on the SD card, I found that the Zynq would repeatedly reboot without giving any output on the serial console. In order to debug this you can load the SPL with JTAG instead of through the SD card. I disabled optimization for size in the configuration in order to make debugging easier. 

Vivado hosts a gdb server when you open the Hardware Manager, on port 3000. This makes it very easy to load the SPL with GDB.

```
gdb-multiarch spl/uboot-spl
tar ext :3000
load
c
```

## Serial port issue

Using GDB and stepping through the code I found it was panicking when trying to initialize the serial port. This was happening as no serial driver had been selected. To resolve this I went to `Device Drivers -> Serial Drivers` and enabled `Cadence (Xilinx Zynq) UART support` then set the option `Select which UART will provide the debug UART` to use the Zynq UART.

I found it is necessary to set the `Base address of UART` to `0xE0000000` for UART0 (see the Zynq reference manual) which I am using on my board and the `UART input clock` to `100000000` (this is project specific, check the settings of the Processing System block in Vivado). I also set `Specify the port number used for console` to true and `UART used for console` to 0.

At this point the SPL was now displaying a prompt on my console, and I was able to load the `boot.bin` from the SD card too.
```
<debug_uart>
Debug uart enabled

U-Boot SPL 2020.10-dirty (Oct 30 2020 - 18:19:22 +0000)
Unsupported boot mode selected
### ERROR ### Please RESET the board ###
```
I still need some further configuration to load U-Boot proper, but it was good to see this output at least.