# Embedded Graphics Menu

[Git Repo](https://github.com/flaminggoat/embedded-graphics-menu)

![](https://raw.githubusercontent.com/flaminggoat/embedded-graphics-menu/master/doc/examples.png)

I have been trying out using rust on cortex-m microcontrollers. There is a great 2D graphics library called [embedded-graphics](https://github.com/jamwaffles/embedded-graphics) which can be used with many displays.

My menu library builds on top of embedded graphics to provide a simple menu that can be used in various projects.

In order to keep the flexibility that embedded-graphics provides, I defined the Menu using [generics](https://doc.rust-lang.org/rust-by-example/generics.html), so the menu can be used with displays that have various bit depths of colour or black and white LCDs. 

```rust
pub struct Menu<'a, C, F, S>
where
    C: PixelColor,
    F: Font,
    S: ArrayLength<MenuEntry<'a>>,
{
    title: &'a str,
    highlighted_option: u8,
    selected: bool,
    redraw: bool,
    size: Size,
    options: MenuOptions<C, F>,
    structure: GenericArray<MenuEntry<'a>, S>,
    last_keys: Keys
}
```

The menu is instantiated with an array of menu entry structs which is passed to the constructor.

```rust
let menu_structure = GenericArray::from([
    MenuEntry {
        l: "Start",
        t: EntryType::Select,
    },
    MenuEntry {
        l: "Sound on",
        t: EntryType::Bool(false),
    },
    MenuEntry {
        l: "Volume",
        t: EntryType::I32((-3, -10, 10)),
    },
]);
```